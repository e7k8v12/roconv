import unittest
from roconv import *

class RomanTestCase(unittest.TestCase):

    test_values = tuple(zip(
        (36, 15, 1999, 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),
        ('XXXVI', 'XV', 'MCMXCIX', 'M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
    ))

    def test_int_to_roman(self):
        for arabic, roman in self.test_values:
            self.assertEqual(int_to_roman(arabic),roman)

    def test_roman_to_int(self):
        for arabic, roman in self.test_values:
            self.assertEqual(roman_to_int(roman), arabic)

if __name__ == '__main__':
    unittest.main(verbosity=3)