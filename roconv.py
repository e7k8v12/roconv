import argparse
import re

test_values = tuple(zip(
    (1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),
    ('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
))


def int_to_roman(rest):

    res = []
    for arabic, roman in test_values:
        if rest >= arabic:
            res.append(roman * (rest // arabic))
            rest = rest % arabic
    return ''.join(res)


def roman_to_int(roman_num):
    res = i = 0
    for arabic, roman in test_values:
        while roman_num[i:i+len(roman)] == roman:
            res += arabic
            i += len(roman)
    return res


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input_value", help="Value to convert")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-fr", "--force_to_roman", action="store_true", help="Force conversion to roman")
    group.add_argument("-fa", "--force_to_arabic", action="store_true", help="Force conversion to arabic")
    args = parser.parse_args()

    args.input_value = args.input_value.upper()

    roman_re = '^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$'

    if args.force_to_roman:
        try:
            value = int(args.input_value)
            print(int_to_roman(value))
        except ValueError:
            raise ValueError('Input must be integer.')
    elif args.force_to_arabic:
        if re.match(roman_re, args.input_value):
            print(roman_to_int(str(args.input_value)))
        else:
            raise ValueError('Wrong roman number.')
    elif re.match(roman_re, args.input_value):
        print(roman_to_int(str(args.input_value)))
    else:
        try:
            value = int(args.input_value)
            print(int_to_roman(value))
        except ValueError:
            raise ValueError('Input must be roman or integer.')

